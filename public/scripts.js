function Convert() {
    var input = document.getElementById("input").value;
    var unit  = document.getElementById("unit").value;
    
     
    // Input Wh / km
    if (unit == 0) {
    var mpkwh = Math.round( ( (1 / input ) * 0.621371 * 1000) * 100) / 100; 
    var whpm  = Math.round( input / 0.621371 );
    var kmpkwh  = Math.round( (1 / input) * 1000 * 100) / 100; 
    document.getElementById("results").innerText = mpkwh + " miles/kWh  |  " + whpm + " Wh/mile  |  " + kmpkwh + " km/kWh"; 
    }
    // Input Wh / mile
    else if (unit == 1) {
    var mpkwh = Math.round( ( (1 / input ) * 1000 ) * 100) / 100; 
    var whpkm  = Math.round( input * 0.621371 ); 
    document.getElementById("results").innerText = mpkwh + " miles/kWh  |  " + whpkm + " Wh/km"; 
    }
    // Input miles / kWh
    else if (unit == 2) {
    var whpm  = Math.round( (1 / input) * 1000);
    var whpkm = Math.round( (1 / input) * 0.621371 * 1000);
    document.getElementById("results").innerText = whpm + " Wh/mile  |  " + whpkm + " Wh/km";
    }
    // Input km / kWh
    else if (unit == 3) {
    var whpkm = Math.round( (1 / input) * 1000);
    var mpkwh = Math.round( input / 0.621371 * 100 ) / 100;
    document.getElementById("results").innerText = whpkm + " Wh/km  |  " + mpkwh + " miles/kWh";
    }


}